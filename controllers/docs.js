const express = require('express');
const router = express.Router();
const querystring = require('querystring')
const Document = require('../models/document');
const User = require('../models/user');
const createError = require('http-errors')
const auth = require('../utils/auth')



router.get('/tags', auth, (req, res, next)=> {
  let tags = []
  Document.find((err, docs) => {
    if (err) return next(createError(500, err))
    if (!docs) return next(createError(500, 'No docs'))
    docs.forEach(doc => {
      doc.tags.forEach(tag => {
        if (!tags.includes(tag)) tags.push(tag)
      })
    })
    res.send(tags.join(', '))
  })
})

router.post('/document', auth, function (req, res, next) {
  let name = req.body.name
  Document.findOne({name: {"$regex" : name, "$options" : '-i'}}, (err, doc) => {
    res.send(doc.content)
  })
});

router.get('/document/by-tag', auth, function (req, res, next) {
  let tag = req.query.tag
  console.log('tag:', tag)
  Document.find({ tags: { $elemMatch: {"$regex" : tag, "$options" : '-i'} } }, (err, docs) => {
    if (err) next(createError(500, err))
    res.send(docs.map(doc => doc.name).join(', '))
  })
});

router.post('/get-documents-name-by-content', auth, function (req, res, next) {
  let content = req.body.content
  Document.find((err, docs) => {
    if (err) next(err)
    let answer = []
    docs.forEach(doc => {
      console.log(doc.content.toLowerCase(), content.toLowerCase())
      if (doc.content && doc.content.toLowerCase().includes(content.toLowerCase())) answer.push(doc.name)
    })
    res.send(answer)
  })
});

router.get('/create-document', auth, function (req, res) {
  let doc = new Document({
    name: 'Введение',
    content: 'haha',
    tags: ['1', '2', '3']
  })
  doc.save(function (err, doc) {
    res.send(doc)
  })

});

module.exports = router;