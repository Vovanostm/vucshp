const express = require('express');
const router = express.Router();
const querystring = require('querystring')
const Document = require('../models/document');
const User = require('../models/user');
const createError = require('http-errors')
const auth = require('../utils/adminAuth')
const log = require('debug')('admin')

const adminAuth = (req, res, next) => {
    if (req.session.user) {
        next()
    }
    else {
        res.render('login')
    }
}


router.post('/users/auth', adminAuth, function (req, res, next) {

    let telegramId = req.body.telegramId
    console.log(telegramId)
    User.findOne({ telegramId: telegramId }, (err, user) => {
        if (err || !user) return next(createError(500, err || 'Такой пользователь не найден'))
        user.isAuth = !user.isAuth
        user.save((err, user) => {
            if (err && !user) return next(createError(500, err || 'Такой пользователь не найден'))
            res.send(`done, ${user}`)
        })
    })
});

router.post('/users/remove', adminAuth, function (req, res, next) {
    let telegramId = req.body.telegramId
    console.log(telegramId)
    User.findOneAndRemove({ telegramId: telegramId }, (err, user) => {
        if (err || !user) return next(createError(500, err || 'Такой пользователь не найден'))
        res.send('ok')
    })
});

router.post('/login', (req, res) => {
    let { username, password } = req.body
    console.log('username:', username, 'password:', password)
    if (username === 'vucshp' && password === 'qualityISeverything')
        req.session.user = username
    res.redirect('/admin')
})

router.get('/', adminAuth, function (req, res) {
    User.find((err, users) => {
        log('err:', err)
        if (err) return next(createError(500, err))
        res.render('admin', {
            users: users
        })
    })
});

router.get('/docs', adminAuth, function (req, res) {
    Document.find((err, docs) => {
        if (err) return next(createError(500, err))
        res.render('docs', {
            docs: docs
        })
    })
});


router.get('/docs/create', adminAuth, function (req, res, next) {
    let id = req.query.id
    Document.findById(id, (err, doc) => {
        if (err) return next(createError(500, err))
        if (!doc) doc = {}
        res.render('docs-edit', {
            doc: doc
        })
    })
});


router.get('/docs/edit', adminAuth, function (req, res, next) {
    let id = req.query.id
    Document.findById(id, (err, doc) => {
        if (err) return next(createError(500, err))
        if (!doc) doc = {}
        res.render('docs-edit', {
            doc: doc
        })
    })
});


router.post('/docs/remove', adminAuth, async function (req, res, next) {
    let id = req.body.docId
    await Document.findByIdAndRemove(id).exec()
    res.send('ok')
});

router.get('/docs/change', adminAuth, function (req, res, next) {
    let doc = req.body
    Document.findOneAndUpdate(doc.id, doc, (err, doc) => {
        if (err) return next(createError(500, err))
        if (!doc) doc = {}
        res.send('ok')
    })
});

router.post('/docs', adminAuth, function (req, res, next) {
    let newDoc = req.body
    console.log(newDoc)
    if (newDoc._id) {
        Document.findById(newDoc._id, (err, doc) => {
            if (err) return next(createError(500, err))
            if (!doc) doc = new Document()
            Object.keys(newDoc).forEach(key => {
                if (key!='_id') doc[key] = newDoc[key]
            })
            doc.content = newDoc.keyIdea +'\n' + newDoc.description + '\n' +  newDoc.FAQ
            doc.tags = newDoc.tags.split(',')
            doc.blankSections = newDoc.blankSections.split(',')
            doc.save((err, doc) => {
                if (err) return next(createError(500, err))
                console.log(doc)
                res.redirect('/admin/docs')
            })
        })
    } else {
        let doc = new Document()
        Object.keys(newDoc).forEach(key => {
            if (key!='_id') doc[key] = newDoc[key]
        })
        doc.tags = newDoc.tags.split(',')
        doc.blankSections = newDoc.blankSections.split(',')
        doc.save((err, doc) => {
            if (err) return next(createError(500, err))
            console.log(doc)
            res.redirect('/admin/docs')
        })
    }
});


module.exports = router;