const express = require('express');
const router = express.Router();
const User = require('../models/user');
const createError = require('http-errors')


router.post('/user', function (req, res, next) {
  let name = req.body.name
  let telegramId = req.body.telegramId
  console.log(req.body, name, telegramId)
  user = new User({
      name,
      telegramId
  })
  user.save(function(err, user) {
      if (err) return next(createError(500, err))
      else res.send('ok')
  })
});

module.exports = router;