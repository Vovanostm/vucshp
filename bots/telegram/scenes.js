const Extra = require('telegraf/extra')
const Markup = require('telegraf/markup')
const session = require('telegraf/session')
const Scene = require('telegraf/scenes/base')
const Docs = require('../../lib/docs')
const Stage = require('telegraf/stage')
const User = require('../../models/user')

const {
    enter,
    leave
} = Stage


const authMiddleware = async (ctx, next) => {

    let user = {}

    const telegramId = ctx.from.id

    user = await User.findOne({
        telegramId
    }).exec()

    if (!user) {

        return ctx.reply('⛔ Пользовтель с вашим id не найден',
            Markup.keyboard(
                [
                    ['👤\nРегистрация', '📞\nКонтакты', '🔗\nGoogle']
                ]
            ).resize().extra())
    }

    if (!user.isAuth) {

        return ctx.reply('⛔ Доступ к функциям библиотеки только для подтвержённых пользователей. Напишите в контакты',
            Markup.keyboard(
                [
                    ['📞\nКонтакты', '🔗\nGoogle']
                ]
            ).resize().extra())
    }

    next()
}



let tags = []
let sections = []

let myContacts = `Если что-то пошло не так, обратитесь к Владимиру Шеламову\n
8 929 684 57 23\n
https://bitrix.informatics.ru/company/personal/user/42/`

// Greeter scene
const library = new Scene('library')


async function printDocsNames(ctx) {
    docs = ctx.session.docsNames || []

    let buttons = []

    docs.forEach(doc => {
        buttons.push([Markup.callbackButton(`${doc}`, `${doc}`)])
    })

    ctx.reply('Выберите совет из списка:', Extra.markdown().markup(
        Markup.inlineKeyboard(buttons).resize()))
}


async function addActions() {



    tags = await Docs.getTags()

    sections = await Docs.getBlankSections()

    documents = await Docs.getDocumentsNames()


    library.enter(authMiddleware, (ctx) => {

        ctx.session.action = 'findByPhrase'
    
        ctx.reply(
            'Введите фразу или выберите режим поиска',
            Markup.keyboard(
                [
                    ['#⃣ Поиск по тегу', '🔖 Поиск по бланку']
                ]
            ).resize().extra())
    })
    
    library.hears('#⃣ Поиск по тегу', async (ctx) => {
    
        let buttons = []
    
        tags.forEach(tag => {
            buttons.push([Markup.callbackButton(`# ${tag}`, `# ${tag}`)])
        })
    
        ctx.reply('Выберите тег из списка',
            Extra.markdown().markup(
                Markup.inlineKeyboard(buttons).resize()
            )
        )
    })
    
    
    library.hears('🔖 Поиск по бланку', async (ctx) => {
    
        let buttons = []
    
        sections.forEach(section => {
            buttons.push([Markup.callbackButton(`🔖 ${section}`, `🔖 ${section}`)])
        })
    
        ctx.reply('Выберите раздел бланка из списка',
            Extra.markdown().markup(
                Markup.inlineKeyboard(buttons).resize()
            )
        )
    })



    tags.forEach(tag => {

        library.action(`# ${tag}`, async (ctx) => {

            ctx.session.action = 'findByTag'

            let docs = (await Docs.getDocumentsByTag(tag)).map(doc => doc.name)

            ctx.session.docsNames = docs

            await printDocsNames(ctx)
        })
    })

    sections.forEach(section => {

        library.action(`🔖 ${section}`, async (ctx) => {

            ctx.session.action = 'findBySection'

            let docs = (await Docs.getDocumentsBySection(section)).map(doc => doc.name)

            ctx.session.docsNames = docs

            await printDocsNames(ctx)
        })
    })


    documents.forEach(docName => {

        library.action(`${docName}`, async (ctx) => {

            let buttons = [
                Markup.callbackButton('+', '+'),
                Markup.callbackButton('~', '~'),
                Markup.callbackButton('-', '-'),
            ]

            let doc = (await Docs.getDocumentByName(docName))



            ctx.session.documentName = docName

            await ctx.replyWithMarkdown(`*${docName}*`, Markup.keyboard(
                [
                    ['📚\nЕщё', '📖\nБиблиотека', 'ℹ\nМеню', '📞'],
                ]
            ).resize().extra())

            await ctx.replyWithMarkdown(`*Ключевая идея:*\n${doc.keyIdea}\n\n`
        )
            await ctx.replyWithMarkdown(`*Описание:*\n${doc.description}\n\n`)

            await ctx.replyWithMarkdown(`*Вопрос-Ответ:*\n${doc.FAQ}\n\n`)

            await ctx.replyWithMarkdown(`*Пункт бланка*\n${doc.blank}\n\n`)

            await ctx.replyWithMarkdown('Пожалуйста, оцените статью',
                Extra.markdown().markup(
                    Markup.inlineKeyboard(buttons)))

        })
    })

    let actions = ['+', '~', '-']

    actions.forEach(action => {

        library.action(`${action}`, async (ctx) => {

            await Docs.markDocument(ctx.session.documentName, action)

            await ctx.replyWithMarkdown('Спасибо!, Ваш голос учтён')

        })
    })


    library.hears('📚\nЕщё', async (ctx) => {
        printDocsNames(ctx)
    })

    library.hears('ℹ\nМеню', async (ctx) => {
        ctx.reply('Добро пожаловать в ВУЦ Школы программистов',
            Markup.keyboard(
                [
                    ['📖\nБиблиотека', '📞\nКонтакты', '🔗\nGoogle']
                ]
            ).resize().extra())
    })

    library.hears('ℹ\nМеню', async (ctx) => {
        ctx.reply('Добро пожаловать в ВУЦ Школы программистов',
            Markup.keyboard(
                [
                    ['📖\nБиблиотека', '📞\nКонтакты', '🔗\nGoogle']
                ]
            ).resize().extra())
    })


    library.hears('📖\nБиблиотека', enter('library'))
    library.hears('📞\nКонтакты', enter('contacts'))
    library.hears('📞', enter('contacts'))
    library.hears('🔗\nGoogle', enter('google'))
    library.hears('👤\nРегистрация', enter('registration'))


    library.on('text', async (ctx) => {

            ctx.session.action = 'findByPhrase'

            let phrase = ctx.message.text

            let docs = (await Docs.getDocumentsByPhrase(phrase)).map(doc => doc.name.trim())

            ctx.session.docsNames = docs

            if (!docs) await ctx.reply('Ничего не нашлось')
            else await printDocsNames(ctx)
    })
}





const contacts = new Scene('contacts')

const printContacts = async (ctx) => {
    ctx.reply(
        myContacts,
        Markup.keyboard(
            [
                ['📖\nБиблиотека', '🔗\nGoogle']
            ]
        ).resize().extra())
}

contacts.enter((ctx) => {
    printContacts(ctx)
})




const google = new Scene('google')

google.enter((ctx) => {
    ctx.reply(
        `Ссылка на оглавление:\nhttps://docs.google.com/spreadsheets/d/1OoSO3B-nIpLMUxv7ERDTgWKzVS_K5StKiDmrEczb7mQ/edit#gid=774570800`,
        Markup.keyboard(
            [
                ['📖\nБиблиотека', '📞\nКонтакты']
            ]
        ).resize().extra())
})


const registration = new Scene('registration')

registration.enter((ctx) => {
    ctx.reply('Пожалуйтса, введите своё фио')
})

registration.on('message', async (ctx, next) => {

    let name = ctx.message.text
    const telegramId = ctx.from.id

    await User.findOneAndUpdate({telegramId}, {$set:{name}}, {upsert:true}).exec()

    ctx.reply('Спасибо за регистрацию!\nОбратитесь к любому сотруднику ВУЦ для предоставления доступа к библиотеки',
        Markup.keyboard(
            [
                ['📖\nБиблиотека', '📞\nКонтакты', '🔗\nGoogle']
            ]
        ).resize().extra())
    next()
}, enter('contacts'))




module.exports = {
    async init() {

        await addActions()

    },
    appendBot(bot) {

        bot.use(async (ctx, next) => {
            console.log('message', ctx.message)
            console.log('session', ctx.session)
            next()
        })

        const stage = new Stage([library, contacts, google, registration])

        bot.use(stage.middleware())

        bot.hears('📖\nБиблиотека', enter('library'))
        bot.hears('📞\nКонтакты', enter('contacts'))
        bot.hears('📞', enter('contacts'))
        bot.hears('🔗\nGoogle', enter('google'))
        bot.hears('👤\nРегистрация', enter('registration'))

    }
}