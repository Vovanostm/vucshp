const Telegraf = require('telegraf')
const session = require('telegraf/session')
const axios = require('axios')
const Extra = require('telegraf/extra')
const Markup = require('telegraf/markup')
const Stage = require('telegraf/stage')
const scenes = require('./scenes')
const docs = require('../../lib/docs')
const Session = require('../../models/session')
const User = require('../../models/user')

const {
    Agent
} = require('http')

let port = process.env.PORT || 5000;

const bot = new Telegraf('486205200:AAEcI2rHxdUbQsXqOqZ-i2A7uZYz3-aCO6w', {
    telegram: {
        apiRoot: 'http://83.166.96.138:3100'
    }
})

bot.use(session())

bot.use(async (ctx, next) => {

    const telegramId = ctx.from.id

    if (!ctx.session.isLoaded) {
        console.log('updated')
        let doc = await Session.findOne({
            telegramId
        }).exec()
        if (!doc) {
            await Session.findOneAndUpdate({
                telegramId
            }, {
                $set: {
                    session: ctx.session
                }
            }, {
                upsert: true
            }).exec()
            doc = {}
            doc.session = ctx.session
        }
        ctx.session = doc.session
        ctx.session.isLoaded = true
    }
    next()
})

bot.use(async (ctx, next) => {

    const telegramId = ctx.from.id

    await Session.findOneAndUpdate({
        telegramId
    }, {
        $set: {
            session: ctx.session
        }
    }, {
        upsert: true
    }).exec()

    next()
})

scenes.init()
scenes.appendBot(bot)



bot.start(async (ctx) => {
    const telegramId = ctx.from.id
    let changedButton = '📖\nБиблиотека'

    user = await User.findOne({
        telegramId
    }).exec()

    if (!user) {
        changedButton = '👤\nРегистрация'
    }
    ctx.reply('Добро пожаловать в ВУЦ Школы программистов',
        Markup.keyboard(
            [
                [changedButton, '📞\nКонтакты', '🔗\nGoogle']
            ]
        ).resize().extra())
})

// bot.hears('ℹ\nМеню', (ctx) => {
//     ctx.reply('Добро пожаловать в ВУЦ Школы программистов',
//         Markup.keyboard(
//             [['📖\nБиблиотека', '📞\nКонтакты', '🔗\nGoogle']]
//         ).resize().extra())
// })



// bot.hears('📖\nБиблиотека', (ctx) => {
//     ctx.reply('Введите фразу или выберите режим поиска',
//     Markup.keyboard(
//         [['#⃣ Поиск по тегу', '🔖 Поиск по бланку']]
//     ).resize().extra())
// })



// bot.hears('#⃣ Поиск по тегу', (ctx) => {
//     let buttons = [
//         Markup.callbackButton('Наставничество', 'Наставничество'),
//         Markup.callbackButton('Поведение', 'Поведение'),
//     ]
//     ctx.reply('Выберите тег из списка', Extra.markdown().markup(
//         Markup.inlineKeyboard(buttons)))
// })

// bot.action('Наставничество', (ctx) => {
//     let buttons = [
//         Markup.callbackButton('Документ 1', 'Документ 1'),
//         Markup.callbackButton('Документ 2', 'Документ 2'),
//     ]
//     ctx.reply('Выберите совет из списка', Extra.markdown().markup(
//         Markup.inlineKeyboard(buttons)))
// })

// bot.hears('📚\nЕщё', (ctx) => {
//     let buttons = [
//         Markup.callbackButton('Документ 1', 'Документ 1'),
//         Markup.callbackButton('Документ 2', 'Документ 2'),
//     ]
//     ctx.reply('Выберите совет из списка', Extra.markdown().markup(
//         Markup.inlineKeyboard(buttons)))
// })

// bot.action('Документ 1', (ctx) => {
//     let buttons = [
//         Markup.callbackButton('+', '+'),
//         Markup.callbackButton('~', '~'),
//         Markup.callbackButton('-', '-'),
//     ]
//     ctx.reply('Содержимое документа', Extra.markdown()).then(() => {
//         ctx.reply('Оцените статью', Extra.markdown().markup(
//             Markup.inlineKeyboard(buttons),
//         ))
//         ctx.reply(' ', Markup.keyboard(
//             [['📚\nЕщё', 'ℹ\nМеню', '📞']]
//         ).resize().extra())
//     })

//     //ctx.reply(Extra.markdown().markup(Markup.inlineKeyboard(buttons)))
// })






// bot.help((ctx) => ctx.reply('Send me a sticker'))
// bot.on('sticker', (ctx) => ctx.reply('👍'))

// bot.hears('📞\nКонтакты', (ctx) => {
//     ctx.reply('Шеламов Владимир Андреевич',
//     Markup.keyboard(
//         [['📖\nБиблиотека', '🔗\nGoogle']]
//     ).resize().extra())

// })

// bot.hears('📞', (ctx) => {
//     ctx.reply('Шеламов Владимир Андреевич',
//     Markup.keyboard(
//         [['📖\nБиблиотека', '🔗\nGoogle']]
//     ).resize().extra())
// })

// bot.hears('🔗\nGoogle', (ctx) => {
//     ctx.reply('Ссылка на таблицу',
//     Markup.keyboard(
//         [['📖\nБиблиотека', '📞\nКонтакты']]
//     ).resize().extra())
// })

// bot.on('message', (ctx) => {
//     console.log(ctx.session)
// })

// bot.on('callback_query', (ctx) => {
//     console.log(ctx.callback_query)
// })

// bot.on('action', (ctx) => {
//     console.log(ctx.action)
// })


// bot.on('message', (ctx) => {
//     console.log(ctx.message)
//     axios({
//             method: 'post',
//             url: `http://localhost:${port}/docs/get-documents-name-by-content?`,
//             data: {
//                 'content': ctx.message.text
//             }
//         })
//         .then(res => {
//             let docs = res.data
//             if (docs.length) {
//                 let buttons = []
//                 docs.forEach(name => {
//                     buttons.push(Markup.callbackButton(name, name))
//                 });
//                 ctx.reply('Найдены документы:', Markup.inlineKeyboard(
//                     buttons
//                 ).extra())
//             } else {
//                 ctx.reply('Совпадений не найдено')
//             }
//         })
//         .catch(err => {
//             // console.log('err', err)
//             ctx.reply(err.message)
//         })
// })

// bot.action(/.+/, (ctx) => {
//     console.log(`whant get http://localhost:${port}/docs/get-document-by-name?name=${ctx.match[0]}`)
//     // axios.get(`http://localhost:${port}/docs/get-document-by-name?name=${ctx.match[0]}`)
//     //     .then(res => {
//     //         // try {
//     //         //     let doc = res.data
//     //         //     console.log(doc.content)
//     //         //     if (doc.content) {
//     //         //         ctx.replyWithHTML(doc.content, Markup.inlineKeyboard(
//     //         //             Markup.callbackButton('Назад', 'Menu')
//     //         //         ).extra())
//     //         //     }
//     //         //     else {
//     //         //         ctx.reply('Совпадений не найдено')
//     //         //     }
//     //         // } catch (e) {
//     //         //     ctx.reply(e.message)
//     //         // }
//     //     })
//     //     .catch(err => {
//     //         console.log('err', err)
//     //     })
//     axios({
//             method: 'post',
//             url: `http://localhost:${port}/docs/document`,
//             data: {
//                 'name': ctx.match[0]
//             }
//         })
//         .then(res => {
//             let doc = res.data
//             console.log(doc.content)
//             if (doc.content) {
//                 ctx.replyWithHTML(doc.content, Markup.inlineKeyboard(
//                     Markup.callbackButton('Назад', 'Menu')
//                 ).extra())
//             } else {
//                 ctx.reply('Совпадений не найдено')
//             }
//         })
//         .catch(err => {
//             console.log(err)
//             ctx.reply(err.message)
//         })
// })

// bot.hears('Библиотека', (ctx) => ctx.reply('Hey there'))
// bot.hears(/buy/i, (ctx) => ctx.reply('Buy-buy'))

bot.startPolling()

module.exports = bot