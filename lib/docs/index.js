const mammoth = require("mammoth");
const XLSX = require("xlsx");
const fs = require('fs');
const path = require('path');
const Document = require('../../models/document')



module.exports = {
    async getTags() {
        let tags = {}
        let docs = await Document.find({}).exec()
        docs.map(doc => {
            doc.tags.forEach(tag => {
                tags[tag] = tag
            })
        })
        return Object.keys(tags)
    },

    async getBlankSections() {
        let sections = {}
        let docs = await Document.find({}).exec()
        docs.map(doc => {
            doc.blankSections.forEach(s => {
                sections[s] = s
            })
        })
        console.log('sections', sections)
        return Object.keys(sections)
    },

    async getDocumentsByTag(tag) {
        return await Document.find({
            tags: {
                $elemMatch: {
                    "$regex": tag,
                    "$options": '-i'
                }
            }
        }).exec()
    },

    async getDocumentsBySection(section) {
        return await Document.find({
            blankSections: {
                $elemMatch: {
                    "$regex": section,
                    "$options": '-i'
                }
            }
        }).exec()
    },

    async getDocumentByName(name) {
        let content = ''
        let document = await Document.findOne({
            name
        }).exec()
        content = `*${document.name}*\n\n`
        content += `*Ключевая мысль*\n${document.keyIdea}\n\n`
        content += `*Описание*\n${document.description}\n\n`
        content += `*Вопрос-ответ*\n${document.FAQ}\n\n`
        content += `*Пункт бланка*\n${document.blank}`
        return {
            name: document.name,
            keyIdea: document.keyIdea,
            description: document.description,
            FAQ: document.FAQ,
            blank: document.blank,
            content
        }
    },

    async getDocumentsByPhrase(phrase) {


        return Document.find({
            content: {
                "$regex": phrase,
                "$options": '-i'
            }
        }, ['name']).exec()
    },

    async getDocumentsNames(name) {
        return (await Document.find({}).exec()).map(doc => doc.name.trim())
    },


    async markDocument(name, mark) {
        console.log(mark)
        doc = await Document.findOne({
            name
        }).exec()
        let vote = {}
        if (doc.vote) vote = doc.vote
        if (vote[mark]) vote[mark]++
            else vote[mark] = 1
        console.log(vote)
        await Document.findOneAndUpdate({
            name
        }, {
            $set: {
                vote
            }
        }).exec()
        return name
    },

    async parseDocuments(dir) {

        fs.readdir(dir, async (err, files) => {
            files.forEach(async (file) => {
                mammoth.extractRawText({
                        path: path.join(dir, file)
                    })
                    .then(function (result) {
                        let text = result.value; // The raw text
                        let lines = text.split('\n')

                        let doc = {}
                        doc.name = lines[0].trim()

                        console.log(doc.name)

                        let splitted
                        let next

                        let isKeyIdea = false
                        let isDescription = false
                        let isFAQ = false
                        let isBlankSection = false

                        let headers = ['Ключевая мысль:', 'Описание:', 'Вопрос-ответ:', 'Пункт бланка:']

                        let splitter = new RegExp('Ключевая мысль:|Описание:|Вопрос-ответ:|Пункт бланка:', 'gi')

                        let keys = ['keyIdea', 'description', 'FAQ', 'blankSections']

                        let validKeys = ['name']

                        keys.forEach((key, index) => {
                            if (text.indexOf(headers[index]) >= 0) validKeys.push(key)
                        })

                        let paragraphs = text.split(splitter)

                        paragraphs.forEach((text, id) => {
                            doc[validKeys[id]] = text.trim()
                        })

                        console.log(doc)


                        doc.content = doc.keyIdea + '\n' + doc.description + '\n' + doc.FAQ

                        Document.findOneAndUpdate({
                            name: doc.name
                        }, {
                            $set: {
                                name: doc.name,
                                content: doc.content,
                                description: doc.description,
                                keyIdea: doc.keyIdea,
                                FAQ: doc.FAQ
                            }
                        }, {
                            upsert: true
                        }, function (err, doc) {
                            if (err) return console.log(err)
                        })
                    })
                    .done();
            });
        })

    },

    async parseTable(table) {
        var workbook = XLSX.readFile(table);
        let sheet = workbook.Sheets['Оглавление']
        // console.log(sheet)
        for (let i = 0; i < 100; i++) {
            if (sheet['A'+i])  {
                let name = sheet['A'+i].v
                let blankSections = sheet['B'+i].v.split(', ')
                let tags = sheet['D'+i].v.split(', ')
                Document.findOneAndUpdate({name}, {$set: {
                    blankSections,
                    tags
                }}).exec()
            }
        }
    }
}