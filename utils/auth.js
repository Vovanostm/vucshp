const User = require('../models/user');
const createError = require('http-errors')
const auth = function (req, res, next) {
    const telegramId = req.body.telegramId || req.query.telegramId
    console.log('telegramId', telegramId)
    User.findOne({ telegramId }, (err, user) => {
        if (err) return next(createError(500, err))
        if (!user) return next(createError(404, 'Пользователь не найден'))
        if (!user.isAuth) return next(createError(401, 'Доступ запрещён. Запросите доступ, для этого перейдите в контакты'))
        next()
    })
}

module.exports = auth