const mongoose = require('mongoose');
const crypto = require('crypto');

const Schema = mongoose.Schema,
  ObjectId = Schema.ObjectId;

let userSchema = new Schema({
  id: ObjectId,
  telegramId: {
      type: String,
      unique: true,
      required: true
  },
  name:{
      type: String,
      unique: true,
      required: true
  },
  isAuth: {
    type: Boolean,
    default: false
  }
});

module.exports = mongoose.model('user', userSchema);