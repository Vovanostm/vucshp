const mongoose = require('mongoose');

const Schema = mongoose.Schema

let schema = new Schema({
    telegramId: {
        type: String,
        unique: true,
        required: true
    },
    session: {
        type: Object,
    }
});

module.exports = mongoose.model('telegramSessions', schema);