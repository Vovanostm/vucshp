const mongoose = require('mongoose');
const crypto = require('crypto');

const Schema = mongoose.Schema,
  ObjectId = Schema.ObjectId;

let documentSchema = new Schema({
  id: ObjectId,
  name:{
      type: String,
      unique: true,
      required: true
  },
  keyIdea: String,
  description: String,
  FAQ: String,
  blank: String,
  tags: Array,
  blankSections: Array,
  content: String,
  vote: Object
});

module.exports = mongoose.model('document', documentSchema);